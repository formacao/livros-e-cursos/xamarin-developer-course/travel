﻿using External.Endpoints.Interfaces;

namespace External.Endpoints
{
    internal class Endpoints : IEndpoints
    {
        public string SearchVenues => "venues/search";
    }
}