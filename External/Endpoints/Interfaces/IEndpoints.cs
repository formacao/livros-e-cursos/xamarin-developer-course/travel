﻿namespace External.Endpoints.Interfaces
{
    public interface IEndpoints
    {
        string SearchVenues { get; }
    }
}