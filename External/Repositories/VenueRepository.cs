﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using External.ApiClients.Interfaces;
using External.Endpoints.Interfaces;
using External.JsonMappers.Interfaces;
using UseCases.Environment.Interfaces;
using UseCases.Models;
using UseCases.Repositories;

namespace External.Repositories
{
    internal class VenueRepository : IVenueRepository
    {
        private const string PlacesApiUrl = @"https://api.foursquare.com/v2";
        
        private readonly IApiClient _apiClient;
        private readonly IEndpoints _endpoints;
        private readonly IEnvironment _environment;
        private readonly IJsonMapper<IEnumerable<Venue>> _mapper;
        
        
        public VenueRepository(IApiClient apiClient, IEndpoints endpoints, IEnvironment environment,  IJsonMapper<IEnumerable<Venue>> mapper)
        {
            _apiClient = apiClient;
            _endpoints = endpoints;
            _environment = environment;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Venue>> GetVenuesNearAtAsync(double latitude, double longitude)
        {
            var data = new
            {
                ll = $"{latitude},{longitude}",
                client_id = _environment.PlacesClientId,
                client_secret = _environment.PlacesClientSecret,
                v = DateTime.Now.ToString("yyyyMMdd")
            };
            var serializedBody = await _apiClient.GetAsync(_endpoints.SearchVenues, data);
            return _mapper.Map(serializedBody);
        }
    }
}
