﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using External.ApiClients.Interfaces;

namespace External.ApiClients
{
    internal class ApiClient : IApiClient
    {

        private readonly HttpClient _client;

        public ApiClient()
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri("https://api.foursquare.com/v2/")
            };
        }
        
        public async Task<string> GetAsync(string endpoint, object urlData)
        {
            var uri = $"{endpoint}?{ToQueryString(urlData)}";
            var result = await _client.GetAsync(uri);
            result.EnsureSuccessStatusCode();
            var serializedResult = await result.Content.ReadAsStringAsync();
            return serializedResult;
        }

        public async Task<string> PostAsync(string endpoint, string serializedJson)
        {
            var content = new StringContent(serializedJson,   Encoding.UTF8, "application/json");
            var result = await _client.PostAsync(endpoint, content);
            result.EnsureSuccessStatusCode();
            var serializedResult = await result.Content.ReadAsStringAsync();

            return serializedResult;
        }
        
        private string ToQueryString(object obj)
        {
            var result = new List<string>();
            var properties = obj.GetType().GetProperties().Where(p => p.GetValue(obj) != null);
            foreach (var property in properties)
            {
                var value = property.GetValue(obj);
                if (value is not string && value is IEnumerable enumerable)
                {
                    var encodedValues = from object item in enumerable select $"{property.Name}[]={HttpUtility.UrlEncode(item.ToString())}";
                    result.AddRange(encodedValues);
                }
                else
                {
                    result.Add($"{property.Name}={HttpUtility.UrlEncode(value.ToString())}");
                }
            }

            return string.Join("&", result.ToArray());
        }
    }
}