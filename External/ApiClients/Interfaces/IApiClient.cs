﻿using System.Threading.Tasks;

namespace External.ApiClients.Interfaces
{
    public interface IApiClient
    {
        Task<string> GetAsync(string endpoint, object urlData);
        Task<string> PostAsync(string endpoint, string serializedJson);
    }
}