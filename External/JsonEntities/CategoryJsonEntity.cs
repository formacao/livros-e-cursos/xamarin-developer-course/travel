﻿using System.Text.Json.Serialization;

namespace External.JsonEntities
{
    internal class CategoryJsonEntity
    {
        [JsonPropertyName("id")]
        public string id { get; set; }
        
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        [JsonPropertyName("pluralName")]
        public string PluralName { get; set; }
        
        [JsonPropertyName("shortName")]
        public string ShortName { get; set; }
    }
}