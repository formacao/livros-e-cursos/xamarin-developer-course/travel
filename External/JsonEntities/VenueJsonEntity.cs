﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace External.JsonEntities
{
    internal class VenueJsonEntity
    {
        [JsonPropertyName("id")]
        public string VenueId { get; set; }
        
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("categories")]
        public IEnumerable<CategoryJsonEntity> Categories { get; set; }
    }
    
    
}
