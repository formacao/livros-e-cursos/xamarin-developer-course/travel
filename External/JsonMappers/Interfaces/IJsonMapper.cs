﻿namespace External.JsonMappers.Interfaces
{
    public interface IJsonMapper<TModel> where TModel: class
    {
        TModel Map(string json);
        string Map(TModel model);
    }
}