﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using External.JsonEntities;
using External.JsonMappers.Interfaces;
using UseCases.Models;

namespace External.JsonMappers
{
    internal class VenueJsonMapper : IJsonMapper<IEnumerable<Venue>>
    {
        public IEnumerable<Venue> Map(string json)
        {
            var venues = new List<Venue>();
            var document = JsonDocument.Parse(json);
            var venuesElement = document.RootElement.GetProperty("response").GetProperty("venues");

            foreach (var venue in venuesElement.EnumerateArray())
            {
                var jsonEntity = JsonSerializer.Deserialize<VenueJsonEntity>(venue.GetRawText());
                var locationElement = venue.GetProperty("location");
                var category = jsonEntity.Categories.First();
                var address = locationElement.TryGetProperty("address", out var addressElement);
                
                venues.Add(new Venue
                {
                    Name = jsonEntity.Name,
                    CategoryId = category.id,
                    CategoryName = category.Name,
                    Address =address ? addressElement.GetString() : string.Empty,
                    Latitude = locationElement.GetProperty("lat").GetDouble(),
                    Longitude = locationElement.GetProperty("lng").GetDouble(),
                    Distance = locationElement.GetProperty("distance").GetInt32()
                });
            }

            return venues;
        }

        public string Map(IEnumerable<Venue> model)
        {
            throw new NotImplementedException();
        }
    }
}