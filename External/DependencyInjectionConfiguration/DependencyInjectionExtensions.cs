﻿using System.Collections.Generic;
using External.ApiClients;
using External.ApiClients.Interfaces;
using External.Endpoints.Interfaces;
using External.JsonMappers;
using External.JsonMappers.Interfaces;
using External.Repositories;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Models;
using UseCases.Repositories;

namespace External.DependencyInjectionConfiguration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddMappers(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IJsonMapper<IEnumerable<Venue>>, VenueJsonMapper>();
            return serviceCollection;
        }

        public static IServiceCollection AddExternalRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IVenueRepository, VenueRepository>();
            return serviceCollection;
        }

        public static IServiceCollection AddEndpoints(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IEndpoints, Endpoints.Endpoints>();
            return serviceCollection;
        }

        public static IServiceCollection AddApiClient(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IApiClient, ApiClient>();
            return serviceCollection;
        }
    }
}