﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using UseCases.DataStructures;
using UseCases.Models;
using UseCases.UseCases.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostDetailPage : ContentPage
    {
        private readonly Post _post;
        public PostDetailPage(Post post)
        {
            InitializeComponent();
            _post = post;
            ExperienceEntry.Text = _post.Experience;
        }

        private void UpdateButton_OnClicked(object sender, EventArgs e)
        {
            PerformUpdate();
        }

        private async Task PerformUpdate()
        {
            using var serviceScope = App.ServiceScope;
            var serviceProvider = serviceScope.ServiceProvider;
            var postsUseCase = serviceProvider.GetRequiredService<IPostUseCase>();

            var postToInsert = new Post {PostId = _post.PostId, Experience = ExperienceEntry.Text};
            var result = await postsUseCase.SaveAsync(postToInsert);
            if(result.IsSuccess)  await Navigation.PopAsync();
            else
            {
                var failure = (FailureUseCaseResult) result;
                await DisplayAlert("Error", failure.Message, "Ok");
            }
        }

        private void DeleteButton_OnClicked(object sender, EventArgs e)
        {
            PerformDelete();
        }

        private async Task PerformDelete()
        {
            using var serviceScope = App.ServiceScope;
            var serviceProvider = serviceScope.ServiceProvider;
            var postsUseCase = serviceProvider.GetRequiredService<IPostUseCase>();

            var result = await postsUseCase.DeleteAsync(_post.PostId);
            if (result is FailureUseCaseResult failure) await DisplayAlert("Error", failure.Message, "Ok");
            else await Navigation.PopAsync();
        }
    }
}