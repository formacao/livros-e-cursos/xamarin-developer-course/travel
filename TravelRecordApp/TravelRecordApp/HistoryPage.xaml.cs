﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.DataStructures;
using UseCases.Models;
using UseCases.UseCases.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage : ContentPage
    {
        private IEnumerable<Post> _posts;
        public IEnumerable<Post> Posts
        {
            get => _posts;
            set
            {
                _posts = value;
                OnPropertyChanged();
            }
        }
        public HistoryPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadPosts();
        }

        private async Task LoadPosts()
        {
            using var serviceScope = App.ServiceScope;
            var serviceProvider = serviceScope.ServiceProvider;
            var useCase = serviceProvider.GetService<IPostUseCase>();
            var result = await useCase.ListAsync();
            if (result is SuccessUseCaseResult<IEnumerable<Post>> success) Posts = success.Result;
        }

        private void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var listView = sender as ListView;
            var selectedPost = listView.SelectedItem as Post;;
            if (selectedPost == null) return;

            Navigation.PushAsync(new PostDetailPage(selectedPost));
        }
    }
}