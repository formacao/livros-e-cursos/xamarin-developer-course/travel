﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace TravelRecordApp
{
    public partial class App : Application
    {
        private readonly IServiceCollection _serviceCollection;
        private  static ServiceProvider _serviceProvider;
        public static IServiceScope ServiceScope => _serviceProvider.CreateScope();

        public App(IServiceCollection serviceCollection) 
        {
            InitializeComponent();
            _serviceCollection = serviceCollection;
            MainPage = new NavigationPage( new MainPage());
        }

        protected override void OnStart()
        {
            _serviceProvider = _serviceCollection.BuildServiceProvider();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}