﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using UseCases.DataStructures;
using UseCases.Models;
using UseCases.UseCases.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        public MapPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ShowUserLocation()
                .ContinueWith(x => DisplayPostsOnMap());
        }

        private async Task DisplayPostsOnMap()
        {
            using var serviceScope = App.ServiceScope;
            var serviceProvider = serviceScope.ServiceProvider;
            var postsUseCase = serviceProvider.GetRequiredService<IPostUseCase>();
            var result = await postsUseCase.ListAsync();
            if (result is SuccessUseCaseResult<IEnumerable<Post>> success)
            {
                var posts = success.Result;
                LocationsMap.Pins.Clear();
                foreach (var post in posts) LocationsMap.Pins.Add(GetMapPin(post));
            }
            else
            {
                var failure = (FailureUseCaseResult) result;
                await DisplayAlert("Error", failure.Message, "Ok");
            }
        }

        private static Pin GetMapPin(Post post)
        {
            var position = new Position(post.Latitude, post.Longitude);
            var pin = new Pin
            {
                Type = PinType.SavedPin,
                Position = position,
                Label = post.VenueName,
                Address = post.Address
            };
            return pin;
        }

        private async Task ShowUserLocation()
        {
            var location = await Geolocation.GetLastKnownLocationAsync() ?? await Geolocation.GetLocationAsync();
            var locationInformation = new MapSpan(new Position(location.Latitude, location.Longitude), 2, 2);
            LocationsMap.MoveToRegion(locationInformation);
        }
    }
}