﻿using External.DependencyInjectionConfiguration;
using LocalDatabase.DependencyInjectionConfiguration;
using Microsoft.Extensions.DependencyInjection;
using UseCases.DependencyInjectionConfiguration;

namespace TravelRecordApp
{
    public static class Startup
    {
        public static IServiceCollection GetDefaultServiceCollection(string databasePath) =>
            new ServiceCollection()
                .AddEnvironmentVariables()
                .AddLocalDatabaseContext(databasePath)
                .AddRepositories()
                .AddExternalRepositories()
                .AddMappers()
                .AddEndpoints()
                .AddApiClient()
                .AddUseCases();
    }
}