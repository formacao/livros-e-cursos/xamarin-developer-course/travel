﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extensions;
using Microsoft.Extensions.DependencyInjection;
using UseCases.DataStructures;
using UseCases.DataStructures.Interfaces;
using UseCases.Models;
using UseCases.UseCases.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewTravelPage : ContentPage
    {
        public NewTravelPage()
        {
            InitializeComponent();
        }

        private void SaveToolbarItem_OnClicked(object sender, EventArgs e)
        {
            if (venuesListView.SelectedItem is not Venue selectedVenue)
            {
                DisplayAlert("Error", "Please, selected a venue.", "Ok");
                return;
            }
            
            using var serviceScope = App.ServiceScope;
            var post = new Post
            {
                Experience = experienceEntry.Text,
                Address = selectedVenue.Address,
                CategoryId = selectedVenue.CategoryId,
                CategoryName = selectedVenue.CategoryName,
                VenueName = selectedVenue.Name,
                Distance = selectedVenue.Distance,
                Latitude =  selectedVenue.Latitude,
                Longitude = selectedVenue.Longitude
            };
            var postUseCase = serviceScope.ServiceProvider.GetRequiredService<IPostUseCase>();
            postUseCase.SaveAsync(post).RunSafe(AnalyzeSaveResult);
        }

        private void AnalyzeSaveResult(IUseCaseResult result)
        {
            if (result.IsSuccess)
            {
                DisplayAlert("Success", "Post saved successfully", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                var failure = (FailureUseCaseResult) result;
                DisplayAlert("Failure", failure.Message, "Ok");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetAvenuesAsync();
        }

        private async Task GetAvenuesAsync()
        {
            var location = await Geolocation.GetLastKnownLocationAsync() ?? await Geolocation.GetLocationAsync();
            using var serviceScope = App.ServiceScope;
            var venuesUseCase = serviceScope.ServiceProvider.GetRequiredService<IVenueUseCase>();
            var venuesResult = await venuesUseCase.GetVenuesNearAtAsync(location.Latitude, location.Longitude);
            if (venuesResult is SuccessUseCaseResult<IEnumerable<Venue>> successResult) venuesListView.ItemsSource = successResult.Result;
            else
            {
                var failure =  (FailureUseCaseResult)venuesResult;
                await DisplayAlert("Error", failure.Message, "Ok");
            }
        }
    }
}