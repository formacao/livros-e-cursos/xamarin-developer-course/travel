﻿using System.IO;
using LocalDatabase.FluentAPIConfiguration;
using Microsoft.EntityFrameworkCore;
using UseCases.Models;

namespace LocalDatabase
{
    public class LocalDatabaseContext : DbContext
    {
        private readonly string _databaseFullPath;
        public DbSet<Post> Posts { get; set; }

        public LocalDatabaseContext(string databasePath)
        {
            _databaseFullPath = Path.Combine(databasePath, "travel_record.sqlite");
            SQLitePCL.Batteries_V2.Init(); //Initializes SQLite in iOS
            Database.EnsureCreated();
        }
        public LocalDatabaseContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;
            optionsBuilder.UseSqlite($"Filename={_databaseFullPath}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PostConfiguration());
        }
    }
}