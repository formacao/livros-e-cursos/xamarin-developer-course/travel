﻿using LocalDatabase.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Repositories;

namespace LocalDatabase.DependencyInjectionConfiguration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddLocalDatabaseContext(this IServiceCollection serviceCollection, string databasePath)
        {
            serviceCollection.AddScoped((sp) => new LocalDatabaseContext(databasePath));
            return serviceCollection;
        }

        public static IServiceCollection AddFixtureDatabaseContext(this IServiceCollection serviceCollection, string databasePath)
        {
            serviceCollection.AddDbContext<LocalDatabaseContext>(optionsBuilder => optionsBuilder.UseSqlite($"Filename={databasePath};Foreign Keys=False"));

            using var serviceProvider = serviceCollection.BuildServiceProvider();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            databaseContext.Database.EnsureCreated();
            
            return serviceCollection;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IPostRepository, PostRepository>();
            return serviceCollection;
        }
    }
}