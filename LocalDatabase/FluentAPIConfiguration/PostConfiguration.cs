﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UseCases.Models;

namespace LocalDatabase.FluentAPIConfiguration
{
    internal class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(x => x.PostId);
            builder.Property(x => x.Experience).IsRequired();
            builder.Property(x => x.CategoryId).IsRequired();
            builder.Property(x => x.CategoryName).IsRequired();
            builder.Property(x => x.VenueName).IsRequired();
            builder.Property(x => x.Address).IsRequired();
        }
    }
}