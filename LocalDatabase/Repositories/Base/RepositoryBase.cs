﻿using Microsoft.EntityFrameworkCore;

namespace LocalDatabase.Repositories.Base
{
    internal abstract  class RepositoryBase
    {
        protected readonly LocalDatabaseContext Context;

        public RepositoryBase(LocalDatabaseContext context)
        {
            Context = context;
        }
    }
}