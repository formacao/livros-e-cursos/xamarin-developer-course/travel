﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LocalDatabase.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using UseCases.Models;
using UseCases.Repositories;

namespace LocalDatabase.Repositories
{
    internal class PostRepository : RepositoryBase, IPostRepository
    {
        public PostRepository(LocalDatabaseContext context) : base(context)
        {
        }
        
        public async Task<Post> SaveAsync(Post post)
        {
            if (post.PostId <= 0) await Context.Posts.AddAsync(post);
            else Context.Entry(post).State = EntityState.Modified;
            await Context.SaveChangesAsync();
            return post;
        }

        public async Task<Post> GetAsync(int postId) => await Context.Posts.AsNoTracking().Where(x => x.PostId == postId).FirstOrDefaultAsync();
        public async Task<IEnumerable<Post>> ListAsync() => await Context.Posts.AsNoTracking().ToListAsync();
        public async Task DeleteAsync(int postId)
        {
            var post = await Context.Posts.Where(x => x.PostId == postId).FirstOrDefaultAsync();
            if (post == null) return;
            
            Context.Posts.Remove(post);
            await Context.SaveChangesAsync();
        }
    }
}