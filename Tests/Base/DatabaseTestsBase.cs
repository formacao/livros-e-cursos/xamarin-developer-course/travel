﻿using System;
using System.IO;
using System.Text;
using LocalDatabase;
using Microsoft.EntityFrameworkCore;

namespace Tests.Base
{
    public abstract class DatabaseTestsBase : IDisposable
    {
        public string DatabasePath => Path.Combine(Environment.CurrentDirectory, _databaseName);
        private readonly string _databaseName;

        public DatabaseTestsBase()
        {
            _databaseName = $"{Guid.NewGuid()}.sqlite";
            Initialize();
        }

        protected virtual void Initialize()
        {
            
        }
        public void Dispose()
        {
            if(File.Exists(DatabasePath)) File.Delete(DatabasePath);
        }
    }
}