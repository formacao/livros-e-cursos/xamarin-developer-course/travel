﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LocalDatabase;
using LocalDatabase.DependencyInjectionConfiguration;
using LocalDatabase.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Tests.Base;
using UseCases.DependencyInjectionConfiguration;
using UseCases.Models;
using UseCases.Repositories;
using UseCases.UseCases.Interfaces;
using Xunit;

namespace Tests.LocalDatabase.Repositories
{
    public class PostRepositoryDatabaseTests : DatabaseTestsBase
    {
        private IServiceCollection _serviceCollection;

        private readonly IEnumerable<Post> _dummyData = new[]
        {
            new Post {Experience = "Dummy Data 01", Address = "Test", CategoryId = "1", CategoryName = "Test", VenueName = "Dummy"},
            new Post {Experience = "Dummy Data 02", Address = "Test", CategoryId = "1", CategoryName = "Test", VenueName = "Dummy"},
            new Post {Experience = "Dummy Data 03", Address = "Test", CategoryId = "1", CategoryName = "Test", VenueName = "Dummy"},
            new Post {Experience = "Dummy Data 04", Address = "Test", CategoryId = "1", CategoryName = "Test", VenueName = "Dummy"}
        };

        [Fact]
        public async Task GetShouldReturnOneRecordGivenId()
        {
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostRepository>();
            Assert.NotNull(await sut.GetAsync(_dummyData.First().PostId));
        }

        [Fact]
        public async Task ListShouldReturnAllRecords()
        {
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostRepository>();
            var result = await sut.ListAsync();
            Assert.Equal(_dummyData.Count(), result.Count());
        }

        [Fact]
        public async Task SaveShouldAddNewPostGivenNonExistingPost()
        {
            var newDummyData = new Post
            {
                PostId = 0,
                Experience = "New Dummy Data",
                Address = "Test address",
                Distance = 45,
                Latitude = 17.48,
                Longitude = -8.02,
                CategoryId = "13",
                CategoryName = "Restaurant",
                VenueName = "Test Restaurant"
            };
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostRepository>();
            var insertedPost = await sut.SaveAsync(newDummyData);
            var posts = await sut.ListAsync();
            var lastPost = posts.Last();

            Assert.Equal(_dummyData.Count() + 1, posts.Count());
            Assert.Equal(insertedPost.PostId, lastPost.PostId);
            Assert.Equal(insertedPost.Address, lastPost.Address);
            Assert.Equal(insertedPost.Distance, lastPost.Distance);
            Assert.Equal(insertedPost.Latitude, lastPost.Latitude);
            Assert.Equal(insertedPost.Longitude, lastPost.Longitude);
            Assert.Equal(insertedPost.CategoryId, lastPost.CategoryId);
            Assert.Equal(insertedPost.CategoryName, lastPost.CategoryName);
            Assert.Equal(insertedPost.VenueName, lastPost.VenueName);
        }

        [Fact]
        public async Task SaveShouldEditExistingPostGivenExistingPost()
        {
            var indexToChange = 1;
            var newExperience = "Edited Experience";

            var postId = _dummyData.ElementAt(indexToChange).PostId;
            var informationToEdit = new Post
            {
                PostId = postId, 
                Experience = newExperience, 
                Address = "Edited", 
                CategoryId = "2", 
                CategoryName = "Teste", 
                VenueName = "Edited test"
            };
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostRepository>();
            await sut.SaveAsync(informationToEdit);
            var insertedPost = await sut.GetAsync(postId);

            Assert.Equal(newExperience, insertedPost.Experience);
        }

        [Fact]
        public async Task DeleteShouldDeleteExistingPost()
        {
            var indexToDelete = 2;
            var postId = _dummyData.ElementAt(indexToDelete).PostId;
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostRepository>();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            await sut.DeleteAsync(postId);

            Assert.Equal(_dummyData.Count() - 1, databaseContext.Posts.Count());
        }

        [Fact]
        public async Task DeleteShouldDoNothingGivenNonExistingPost()
        {
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostRepository>();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            await sut.DeleteAsync(800);

            Assert.Equal(_dummyData.Count(), databaseContext.Posts.Count());
        }

        protected override void Initialize()
        {
            ConfigureServices();
            InsertDummyData();
        }

        private void ConfigureServices()
        {
            _serviceCollection = new ServiceCollection()
                .AddFixtureDatabaseContext(DatabasePath)
                .AddRepositories();
        }

        private void InsertDummyData()
        {
            using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            databaseContext.Posts.AddRange(_dummyData);
            databaseContext.SaveChanges();
        }
    }
}