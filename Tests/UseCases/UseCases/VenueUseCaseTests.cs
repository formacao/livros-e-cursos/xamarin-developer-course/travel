﻿using System.Collections.Generic;
using System.Threading.Tasks;
using External.ApiClients.Interfaces;
using External.DependencyInjectionConfiguration;
using External.Endpoints.Interfaces;
using LocalDatabase.DependencyInjectionConfiguration;
using Microsoft.Extensions.DependencyInjection;
using Tests.External.Fixtures;
using UseCases.DataStructures;
using UseCases.DataStructures.Interfaces;
using UseCases.DependencyInjectionConfiguration;
using UseCases.Models;
using UseCases.UseCases.Interfaces;
using Xunit;

namespace Tests.UseCases.UseCases
{
    public class VenueUseCaseTests
    {
        public IServiceCollection GetDefaultServiceCollection() => new ServiceCollection()
                .AddEnvironmentVariables()
                .AddTransient<IApiClient, FakeApiClient>()
                .AddMappers()
                .AddExternalRepositories()
                .AddUseCases();

        [Fact]
        public async Task GetVenuesNearAtAsyncShouldReturnNonEmptyList()
        {
            var serviceCollection = GetDefaultServiceCollection()
                .AddTransient<IEndpoints>(sp => new FakeEndpoints {VenuesJsonFileName = "venues.json"});

            await using var serviceProvider = serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IVenueUseCase>();
            var result = await sut.GetVenuesNearAtAsync(0, 0);
            var successResult = (SuccessUseCaseResult<IEnumerable<Venue>>) result;
            
            Assert.NotEmpty(successResult.Result);
            Assert.DoesNotContain(successResult.Result, x => string.IsNullOrEmpty(x.Address) || string.IsNullOrEmpty(x.Name) || x.Distance == default);
        }
    }
}