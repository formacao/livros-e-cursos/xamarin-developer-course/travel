﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LocalDatabase;
using LocalDatabase.DependencyInjectionConfiguration;
using LocalDatabase.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Tests.Base;
using UseCases.DataStructures;
using UseCases.DependencyInjectionConfiguration;
using UseCases.Models;
using UseCases.Repositories;
using UseCases.UseCases;
using UseCases.UseCases.Interfaces;
using Xunit;

namespace Tests.UseCases.UseCases
{
    public class PostUseCaseTests : DatabaseTestsBase
    {

        private IServiceCollection _serviceCollection;

            [Fact]
        public async Task SaveShouldAddNewPost()
        {
            var postToSave = new Post
            {
                Experience = "New post",
                Address = "Dummy Address",
                CategoryId = "18",
                CategoryName = "New Category Test",
                VenueName = "Another Test"
            };
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            var sut = serviceProvider.GetRequiredService<IPostUseCase>();
            var result = (SuccessUseCaseResult<Post>) (await sut.SaveAsync(postToSave));
            var insertedPost = await databaseContext.Posts.AsNoTracking().FirstAsync(x => x.PostId == result.Result.PostId);

            Assert.Equal(result.Result.PostId, insertedPost.PostId);
        }

        [Fact]
        public async Task SaveShouldEditExistingPost()
        {
            var total = await SeedDummyData();
            var postToSave = new Post
            {
                PostId = 1, 
                Experience = "This post should be edited",
                Address = "Dummy Address",
                CategoryId = "18",
                CategoryName = "New Category Test",
                VenueName = "Another Test"
            };
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            var sut = serviceProvider.GetRequiredService<IPostUseCase>();

            var result = await sut.SaveAsync(postToSave);
            var success = (SuccessUseCaseResult<Post>) result;
            
            Assert.Equal(total, databaseContext.Posts.Count());
            Assert.Equal(postToSave.PostId, success.Result.PostId);
            Assert.Equal(postToSave.Experience, success.Result.Experience);
        }

        [Fact]
        public async Task ListAsyncShouldReturnAllPosts()
        {
            var total = await SeedDummyData();
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IPostUseCase>();
            var result = await sut.ListAsync();
            var successResult = (SuccessUseCaseResult<IEnumerable<Post>>) result;
            
            Assert.Equal(total, successResult.Result.Count());
        }

        [Fact]
        public async Task DeleteAsyncShouldDeleteExistingPost()
        {
            var total = await SeedDummyData();
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            var sut = serviceProvider.GetRequiredService<IPostUseCase>();
            var result = await sut.DeleteAsync(1);

            Assert.True(result.IsSuccess);
            Assert.Equal(total - 1, databaseContext.Posts.Count());
        }

        protected override void Initialize()
        {
            _serviceCollection = new ServiceCollection()
                .AddFixtureDatabaseContext(DatabasePath)
                .AddRepositories()
                .AddUseCases();
        }

        private async Task<int> SeedDummyData()
        {
            await using var serviceProvider = _serviceCollection.BuildServiceProvider();
            var databaseContext = serviceProvider.GetRequiredService<LocalDatabaseContext>();
            var posts = new List<Post>
            {
                new()
                {
                    Experience = "Test 01",
                    Address = "Dummy Address",
                    CategoryId = "3",
                    CategoryName = "Test",
                    VenueName = "Venue Test"
                },
                new()
                {
                    Experience = "Test 02",
                    Address = "Dummy Address",
                    CategoryId = "3",
                    CategoryName = "Test",
                    VenueName = "Venue Test"
                },
                new()
                {
                    Experience = "Test 03",
                    Address = "Dummy Address",
                    CategoryId = "3",
                    CategoryName = "Test",
                    VenueName = "Venue Test"
                }
            };
            await databaseContext.AddRangeAsync(posts);
            await databaseContext.SaveChangesAsync();

            return posts.Count;
        }
    }
}