﻿using System.Threading.Tasks;
using External.ApiClients.Interfaces;
using External.DependencyInjectionConfiguration;
using External.Endpoints.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Tests.External.Fixtures;
using UseCases.DependencyInjectionConfiguration;
using UseCases.Repositories;
using Xunit;

namespace Tests.External.Repositories
{
    public class VenuesRepositoryTests
    {
        
        private IServiceCollection GetDefaultServiceCollection()
        {
            return new ServiceCollection()
                .AddTransient<IApiClient, FakeApiClient>()
                .AddMappers()
                .AddEnvironmentVariables()
                .AddExternalRepositories();
        }
        
        [Fact]
        public async Task GetVenuesNearAtAsyncShouldRetornoData()
        {
            var serviceCollection = GetDefaultServiceCollection()
                .AddTransient<IEndpoints>(sp => new FakeEndpoints {VenuesJsonFileName = "venues.json"});
            
            await using var serviceProvider = serviceCollection.BuildServiceProvider();
            var sut = serviceProvider.GetRequiredService<IVenueRepository>();
            var items = await sut.GetVenuesNearAtAsync(0, 0);
            
            Assert.NotEmpty(items);
            Assert.DoesNotContain(items, x => 
                string.IsNullOrEmpty(x.Address) || string.IsNullOrEmpty(x.Name) || x.Distance == default ||
                string.IsNullOrWhiteSpace(x.CategoryId) || string.IsNullOrEmpty(x.CategoryName) ||
                x.Latitude == default || x.Longitude == default
                );
        }
    }
}