﻿using System;
using System.IO;
using System.Threading.Tasks;
using External.ApiClients.Interfaces;
using External.JsonMappers.Interfaces;
using UseCases.Models;

namespace Tests.External.Fixtures
{
    public class FakeApiClient : IApiClient
    {
        public async Task<string> GetAsync(string endpoint, object urlData) => await File.ReadAllTextAsync(endpoint);

        public Task<string> PostAsync(string endpoint, string serializedJson)
        {
            throw new InvalidOperationException();
        }
    }
}