﻿using System;
using System.IO;
using External.Endpoints.Interfaces;

namespace Tests.External.Fixtures
{
    public class FakeEndpoints : IEndpoints
    {
        private string _jsonBasePath = Path.Combine(Environment.CurrentDirectory,  "External", "JsonMocks");

        public string VenuesJsonFileName { get; set; }
        public string SearchVenues => Path.Combine(_jsonBasePath, "Venue", VenuesJsonFileName);
    }
}