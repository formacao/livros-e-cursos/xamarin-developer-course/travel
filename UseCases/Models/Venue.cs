﻿namespace UseCases.Models
{
    public class Venue
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Distance { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}