﻿using System.IO;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using UseCases.UseCases;
using UseCases.UseCases.Interfaces;
using System.Text.Json;
using UseCases.Environment.Interfaces;

namespace UseCases.DependencyInjectionConfiguration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddUseCases(this IServiceCollection serviceCollection)
        {
            serviceCollection.
                AddSingleton<IPostUseCase, PostUseCase>()
                .AddSingleton<IVenueUseCase, VenueUseCase>();
            
            return serviceCollection;
        }

        public static IServiceCollection AddEnvironmentVariables(this IServiceCollection serviceCollection)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using var resource = assembly.GetManifestResourceStream("UseCases.Environment.env.json");
            using var reader = new StreamReader(resource);
            var serializedJson = reader.ReadToEnd();
            var deserializedJson = JsonSerializer.Deserialize<Environment.Environment>(serializedJson);

            serviceCollection.AddSingleton<IEnvironment>(sp => deserializedJson);
            
            return serviceCollection;
        }
    }
}