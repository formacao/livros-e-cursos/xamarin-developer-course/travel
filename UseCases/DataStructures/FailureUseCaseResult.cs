﻿using UseCases.DataStructures.Interfaces;

namespace UseCases.DataStructures
{
    public class FailureUseCaseResult : IUseCaseResult
    {
        public bool IsSuccess => false;
        public string Message { get; }

        public FailureUseCaseResult(string message)
        {
            Message = message;
        }
    }
}