﻿using UseCases.DataStructures.Interfaces;

namespace UseCases.DataStructures
{
    public class SuccessUseCaseResult<T> : IUseCaseResult
    {
        public bool IsSuccess => true;
        public T Result { get; }

        public SuccessUseCaseResult(T result)
        {
            Result = result;
        }
    }
    
    public class SuccessUseCaseResult: IUseCaseResult
    {
        public bool IsSuccess => true;
    }
}