﻿namespace UseCases.DataStructures.Interfaces
{
    public interface IUseCaseResult
    {
        public bool IsSuccess { get;}
    }
}