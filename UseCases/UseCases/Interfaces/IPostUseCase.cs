﻿using System.Threading.Tasks;
using UseCases.DataStructures.Interfaces;
using UseCases.Models;

namespace UseCases.UseCases.Interfaces
{
    public interface IPostUseCase
    {
        Task<IUseCaseResult> SaveAsync(Post post);
        Task<IUseCaseResult> ListAsync();
        Task<IUseCaseResult> DeleteAsync(int postId);
    }
}