﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using UseCases.DataStructures.Interfaces;
using UseCases.Models;

namespace UseCases.UseCases.Interfaces
{
    public interface IVenueUseCase
    {
        Task<IUseCaseResult> GetVenuesNearAtAsync(double latitude, double longitude);
    }
}