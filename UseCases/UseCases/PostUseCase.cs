﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UseCases.DataStructures;
using UseCases.DataStructures.Interfaces;
using UseCases.Models;
using UseCases.Repositories;
using UseCases.UseCases.Interfaces;

namespace UseCases.UseCases
{
    internal class PostUseCase : IPostUseCase
    {
        private readonly IPostRepository _postRepository;

        public PostUseCase(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }


        public async Task<IUseCaseResult> SaveAsync(Post post)
        {
            try
            {
                var savedPost = await _postRepository.SaveAsync(post);
                return new SuccessUseCaseResult<Post>(savedPost);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new FailureUseCaseResult(e.Message);
            }
        }

        public async Task<IUseCaseResult> ListAsync()
        {
            var posts = await _postRepository.ListAsync();
            return new SuccessUseCaseResult<IEnumerable<Post>>(posts);
        }

        public async Task<IUseCaseResult> DeleteAsync(int postId)
        {
            try
            {
                await _postRepository.DeleteAsync(postId);
                return new SuccessUseCaseResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new FailureUseCaseResult("Unable to delete post");
            }
        }
    }
}