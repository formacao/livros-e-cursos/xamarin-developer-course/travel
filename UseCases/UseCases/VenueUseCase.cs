﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UseCases.DataStructures;
using UseCases.DataStructures.Interfaces;
using UseCases.Models;
using UseCases.Repositories;
using UseCases.UseCases.Interfaces;

namespace UseCases.UseCases
{
    internal class VenueUseCase : IVenueUseCase
    {
        private readonly IVenueRepository _repository;

        public VenueUseCase(IVenueRepository repository)
        {
            _repository = repository;
        }

        public async Task<IUseCaseResult> GetVenuesNearAtAsync(double latitude, double longitude)
        {
            try
            {
                var result = await _repository.GetVenuesNearAtAsync(latitude, longitude);
                return new SuccessUseCaseResult<IEnumerable<Venue>>(result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new FailureUseCaseResult("Unable to get the venues");
            }
        }
    }
}