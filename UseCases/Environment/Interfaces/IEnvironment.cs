﻿using System;

namespace UseCases.Environment.Interfaces
{
    public interface IEnvironment
    {
        string PlacesClientId { get; }
        string PlacesClientSecret { get; }
    }
}