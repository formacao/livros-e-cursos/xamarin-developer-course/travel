﻿using System;
using UseCases.Environment.Interfaces;

namespace UseCases.Environment
{
    internal class Environment : IEnvironment
    {
        public string PlacesClientId { get; set; }
        public string PlacesClientSecret { get; set; }
    }
}