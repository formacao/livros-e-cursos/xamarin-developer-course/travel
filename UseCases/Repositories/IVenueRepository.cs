﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UseCases.Models;

namespace UseCases.Repositories
{
    public interface IVenueRepository
    {
        Task<IEnumerable<Venue>> GetVenuesNearAtAsync(double latitude, double longitude);
    }
}