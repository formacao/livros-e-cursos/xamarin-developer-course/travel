﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UseCases.Models;

namespace UseCases.Repositories
{
    public interface IPostRepository
    {
        Task<Post> SaveAsync(Post post);
        Task<Post> GetAsync(int postId);
        Task<IEnumerable<Post>> ListAsync();
        Task DeleteAsync(int postId);
    }
}