﻿using System;

namespace CharacterizationTests.DependencyInjection.Mocks
{
    public class SingletonMock : IDisposable
    {
        public static int CreationCounter;
        public static int DisposeCounter;

        public SingletonMock()
        {
            CreationCounter++;
        }

        public void Dispose()
        {
            DisposeCounter++;
        }
    }
}