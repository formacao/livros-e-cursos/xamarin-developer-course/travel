﻿using System;

namespace CharacterizationTests.DependencyInjection.Mocks
{
    public class TransientMock : IDisposable
    {
        public static int CreationCounter;
        public static int DisposeCounter;

        public TransientMock()
        {
            CreationCounter++;
        }

        public void Dispose()
        {
            DisposeCounter++;
        }
    }
}