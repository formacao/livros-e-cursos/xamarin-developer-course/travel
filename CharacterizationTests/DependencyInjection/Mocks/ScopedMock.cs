﻿using System;

namespace CharacterizationTests.DependencyInjection.Mocks
{
    public class ScopedMock : IDisposable
    {
        public static int CreationCounter;
        public static int DisposeCounter;

        public ScopedMock()
        {
            CreationCounter++;
        }

        public void Dispose()
        {
            DisposeCounter++;
        }
    }
}