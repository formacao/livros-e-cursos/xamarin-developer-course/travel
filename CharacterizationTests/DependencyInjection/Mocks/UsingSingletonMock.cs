﻿using System;

namespace CharacterizationTests.DependencyInjection.Mocks
{
    public class UsingSingletonMock : IDisposable
    {
        public static int CreationCounter;
        public static int DisposeCounter;

        public UsingSingletonMock()
        {
            CreationCounter++;
        }

        public void Dispose()
        {
            DisposeCounter++;
        }
    }
}