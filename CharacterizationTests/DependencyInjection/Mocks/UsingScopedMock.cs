﻿using System;

namespace CharacterizationTests.DependencyInjection.Mocks
{
    public class UsingScopedMock : IDisposable
    {
        public static int CreationCounter;
        public static int DisposeCounter;

        public UsingScopedMock()
        {
            CreationCounter++;
        }

        public void Dispose()
        {
            DisposeCounter++;
        }
    }
}