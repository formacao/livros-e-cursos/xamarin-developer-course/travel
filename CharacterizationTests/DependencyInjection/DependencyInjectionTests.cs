﻿using CharacterizationTests.DependencyInjection.Mocks;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace CharacterizationTests.DependencyInjection
{
    public class DependencyInjectionTests
    {
        [Fact]
        public void AddTransientShouldAlwaysCreateAnInstance()
        {
            var serviceScope = new ServiceCollection()
                .AddTransient<TransientMock>()
                .BuildServiceProvider()
                .CreateScope();

            var serviceProvider = serviceScope.ServiceProvider;

            var transient1 = serviceProvider.GetService<TransientMock>();
            var transient2 = serviceProvider.GetService<TransientMock>();
            var transient3 = serviceProvider.GetService<TransientMock>();
            
            serviceScope.Dispose();
            Assert.Equal(3, TransientMock.CreationCounter);
            Assert.Equal(3, TransientMock.DisposeCounter);
            
        }

        [Fact]
        public void AddScopedShouldCreateOneInstancePerScope()
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<ScopedMock>()
                .BuildServiceProvider();

            var scope1 = serviceProvider.CreateScope();

            var scoped1 = scope1.ServiceProvider.GetService<ScopedMock>();
            var scoped2= scope1.ServiceProvider.GetService<ScopedMock>();
            var scoped3 = scope1.ServiceProvider.GetService<ScopedMock>();
            
            scope1.Dispose();

            var scope2 = serviceProvider.CreateScope();
            
            var scoped4 = scope2.ServiceProvider.GetService<ScopedMock>();
            var scoped5= scope2.ServiceProvider.GetService<ScopedMock>();
            var scoped6 = scope2.ServiceProvider.GetService<ScopedMock>();
            
            scope2.Dispose();

            Assert.Equal(2, ScopedMock.CreationCounter);
            Assert.Equal(2, ScopedMock.DisposeCounter);
        }
        
        [Fact]
        public void AddSingletonShouldCreateOneInstance()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<SingletonMock>()
                .BuildServiceProvider();

            var scope1 = serviceProvider.CreateScope();

            var singleton1 = scope1.ServiceProvider.GetService<SingletonMock>();
            var singleton2= scope1.ServiceProvider.GetService<SingletonMock>();
            var singleton3 = scope1.ServiceProvider.GetService<SingletonMock>();
            
            scope1.Dispose();
            

            var scope2 = serviceProvider.CreateScope();

            var singleton4 = scope2.ServiceProvider.GetService<SingletonMock>();
            var singleton5= scope2.ServiceProvider.GetService<SingletonMock>();
            var singleton6 = scope2.ServiceProvider.GetService<SingletonMock>();
            
            scope2.Dispose();

            Assert.Equal(1, SingletonMock.CreationCounter);
            Assert.Equal(0, SingletonMock.DisposeCounter);
            
            serviceProvider.Dispose();
            
            Assert.Equal(1, SingletonMock.DisposeCounter);
        }
        
    }
}