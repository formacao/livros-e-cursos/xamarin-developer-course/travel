﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Extensions
{
    public static class TaskExtensions
    {
        public static async void RunSafe(this Task task, Action<Exception> errorCalback = null)
        {
            try
            {
                await task;
            }
            catch (Exception e)
            {
                errorCalback?.Invoke(e);
            }
        }

        public static async void RunSafe<T>(this Task<T> task, Action<T> completionCallback, Action<Exception> errorCallback = null)
        {
            try
            {
                completionCallback.Invoke(await task);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errorCallback?.Invoke(e);
            }
        }
    }
}